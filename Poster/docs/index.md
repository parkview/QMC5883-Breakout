# QMC5883 3D Magnetic Field Compass Breakout 
  
A small breakout board for the QMC5883 digital compass IC.  Seems to work well and gives a steady and consistent heading. 
  
A bit more project info can be found here: [https://forum.swmakers.org/viewtopic.php?f=9&t=2495](https://forum.swmakers.org/viewtopic.php?f=9&t=2495)  
  
For more documentation visit [https://gitlab.com/parkview/QMC5883-Breakout](https://gitlab.com/parkview/QMC5883-Breakout)
  
## Hardware Features:
---------
#### Version 0.5 Features: 

* 4-Pin Picoblade 1.25mm pitch connector for power and i2C data
* alternative 4-pin 1.27mm pitch pin-header connector 
* M1.2 mounting holes 
* 2 layer 13mm x 11.2mm PCB 
  
  
## Available Software 

![Gitlab URL](images/QMC5883-Breakout_QR_URL.png){ align=right : style="height:100px;width:100px"}
#### Arduino:
* `QMC5883-Compass` - PlatformIO Arduino example code based around DF-Robots library code, giving a Serial output  

![QMC5883](images/QMC5883L_sml.jpg){ align=left : style="height:169px;width:280px"}  
